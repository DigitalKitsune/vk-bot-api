package api

const version = "5.103"

/*Context of all requests*/
type Context struct {
	AccessToken string
	Key         string
	ServerURL   string
	TS          int
	GroupID     int
	PeerID      int
	UserID      int
	MessID      int
}
