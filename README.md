# vk-bot-api -- simple vk api bindings

## Example usage:

```
package main

import (
	"fmt"
	"os"
	"strings"
	vkapi "gitlab.com/DigitalKitsune/vk-bot-api/"
)

func main() {
	var ctx vkapi.Context
	ctx.InitLongPoll("your_group_token", your_group_id)
	routine := func(ctx vkapi.Context, text string) {
		fmt.Printf("%s\n", text)
		if text == "Бот" {
			ctx.SetActivity()
			ctx.Send("Жив и работаю!")
			
			file, err := os.Open("./work.jpeg")
			if err == nil {
				ctx.SendPhoto(file)
				file.Close()
			}
		}
	}
	err := ctx.GroupLongPoll(routine)
	fmt.Printf("%+v\n", err)
}
```