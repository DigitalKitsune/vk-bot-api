package api

import (
	"io/ioutil"
	"net/http"
)

func (ctx *Context) apiRequest(method string, options map[string]string) (body []byte, err error) {
	req, err := http.NewRequest("POST", "https://api.vk.com/method/"+method, nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("v", version)
	q.Add("access_token", ctx.AccessToken)
	for key, value := range options {
		q.Add(key, value)
	}

	req.URL.RawQuery = q.Encode()

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
