package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net/http"
)

/*Send - sends a message*/
func (ctx *Context) Send(message string) (err error) {
	options := map[string]string{
		"peer_id":   fmt.Sprintf("%d", ctx.PeerID),
		"random_id": fmt.Sprintf("%d", rand.Int63()),
		"message":   message}
	_, err = ctx.apiRequest("messages.send", options)
	return err
}

/*SendReply - sends a message*/
func (ctx *Context) SendReply(message string) (err error) {
	options := map[string]string{
		"peer_id":   fmt.Sprintf("%d", ctx.PeerID),
		"random_id": fmt.Sprintf("%d", rand.Int63()),
		"message":   message}
	_, err = ctx.apiRequest("messages.send", options)
	return err
}

/*SendAudioMessage - sends audio message*/
func (ctx *Context) SendAudioMessage(file io.Reader) (err error) {
	serv, err := ctx.docsGetUploadServer("audio_message")
	//fmt.Printf("%s\n", serv)
	if err != nil {
		return err
	}
	resp, err := ctx.uploadFile(serv, file, "audiomessage.ogg")
	//fmt.Printf("%s\n", resp)
	if err != nil {
		return err
	}
	type res struct {
		F string `json:"file"`
	}
	var r res
	err = json.Unmarshal(resp, &r)
	if err != nil {
		return err
	}
	respo, err := ctx.docsSave(r.F, "Voice message")
	options := map[string]string{
		"peer_id":    fmt.Sprintf("%d", ctx.PeerID),
		"random_id":  fmt.Sprintf("%d", rand.Int63()),
		"attachment": respo}
	resp, err = ctx.apiRequest("messages.send", options)
	fmt.Printf("%s\n", resp)
	return err
}

/*SendPhoto - sends photo*/
func (ctx *Context) SendPhoto(file io.Reader) (err error) {
	serv, err := ctx.photosGetUploadServer()
	//fmt.Printf("%s\n", serv)
	if err != nil {
		return err
	}
	resp, err := ctx.uploadFile(serv, file, "photo.jpg")
	if err != nil {
		return err
	}
	type res struct {
		S int    `json:"server"`
		P string `json:"photo"`
		H string `json:"hash"`
	}

	var r res
	err = json.Unmarshal(resp, &r)
	if err != nil {
		return err
	}
	respo, err := ctx.photosSave(r.P, fmt.Sprintf("%d", r.S), r.H)

	options := map[string]string{
		"peer_id":    fmt.Sprintf("%d", ctx.PeerID),
		"random_id":  fmt.Sprintf("%d", rand.Int63()),
		"attachment": respo}
	resp, err = ctx.apiRequest("messages.send", options)
	return err
}

/*SetActivity - sets activity on*/
func (ctx *Context) SetActivity() {
	options := map[string]string{
		"peer_id": fmt.Sprintf("%d", ctx.PeerID),
		"type":    "typing"}
	//resp, err :=
	ctx.apiRequest("messages.setActivity", options)
	//fmt.Printf("%s\n%+v\n", resp, err)
}

func (ctx *Context) uploadFile(url string, file io.Reader, filename string) (out []byte, err error) {

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filename)
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)
	if err != nil {
		return nil, err
	}

	//writer.WriteField("v", version)
	//writer.WriteField("access_token", ctx.AccessToken)

	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	rbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("%s\n", rbody)

	return rbody, nil
}

func getUploadServer(resp []byte) (str string, err error) {
	type doc struct {
		R struct {
			U string `json:"upload_url"`
		} `json:"response"`
	}
	var d doc
	err = json.Unmarshal(resp, &d)
	if err != nil {
		return "", err
	}
	return d.R.U, nil
}

func (ctx *Context) docsGetUploadServer(dType string) (str string, err error) {
	options := map[string]string{
		"peer_id": fmt.Sprintf("%d", ctx.PeerID),
		"type":    dType}
	body, err := ctx.apiRequest("docs.getMessagesUploadServer", options)
	if err != nil {
		return "", err
	}
	return getUploadServer(body)
}

func (ctx *Context) photosGetUploadServer() (str string, err error) {
	options := map[string]string{
		"peer_id": fmt.Sprintf("%d", ctx.PeerID)}
	body, err := ctx.apiRequest("photos.getMessagesUploadServer", options)
	if err != nil {
		return "", err
	}
	return getUploadServer(body)
}

func (ctx *Context) photosSave(photo string, server string, hash string) (str string, err error) {
	options := map[string]string{
		"photo":  photo,
		"server": server,
		"hash":   hash}
	body, err := ctx.apiRequest("photos.saveMessagesPhoto", options)
	if err != nil {
		return "", err
	}
	//fmt.Printf("%s\n", body)
	type res struct {
		R []struct {
			I int `json:"id"`
			O int `json:"owner_id"`
		} `json:"response"`
	}
	var r res
	err = json.Unmarshal(body, &r)
	return fmt.Sprintf("photo%d_%d", r.R[0].O, r.R[0].I), nil
}

func (ctx *Context) docsSave(file string, name string) (str string, err error) {
	options := map[string]string{
		"file":  file,
		"title": name}
	body, err := ctx.apiRequest("docs.save", options)
	if err != nil {
		return "", err
	}
	//fmt.Printf("%s\n", body)
	type res struct {
		R struct {
			T string `json:"type"`
			M struct {
				I int    `json:"id"`
				O int    `json:"owner_id"`
				A string `json:"access_key"`
			} `json:"audio_message"`
		} `json:"response"`
	}
	var r res
	err = json.Unmarshal(body, &r)
	return fmt.Sprintf("%s%d_%d", r.R.T, r.R.M.O, r.R.M.I), nil
}
