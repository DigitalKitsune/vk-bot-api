package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

/*UpdateGoroutine - callback function*/
type UpdateGoroutine func(Context, string)

/*InitLongPoll initialize polling loop*/
func (ctx *Context) InitLongPoll(accessToken string, groupID int) (err error) {
	ctx.AccessToken, ctx.GroupID = accessToken, groupID
	options := map[string]string{"group_id": fmt.Sprintf("%d", groupID)}
	body, err := ctx.apiRequest("groups.getLongPollServer", options)
	fmt.Printf("%s\n", body)
	type answer struct {
		R struct {
			T int    `json:"ts,string"`
			K string `json:"key"`
			S string `json:"server"`
		} `json:"response"`
	}
	var a answer
	err = json.Unmarshal(body, &a)
	fmt.Printf("%+v\n", a)
	if err == nil {
		ctx.Key, ctx.ServerURL, ctx.TS = a.R.K, a.R.S, a.R.T
		ctx.AccessToken, ctx.GroupID = accessToken, groupID
	}
	return err
}

/*GroupLongPoll polling loop*/
func (ctx *Context) GroupLongPoll(routine UpdateGoroutine) (err error) {
	for {
		req, err := http.NewRequest("GET", ctx.ServerURL, nil)
		if err != nil {
			return err
		}

		q := req.URL.Query()
		q.Add("act", "a_check")
		q.Add("key", ctx.Key)
		q.Add("ts", fmt.Sprintf("%d", ctx.TS))
		q.Add("wait", "25")
		req.URL.RawQuery = q.Encode()

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		resp.Body.Close()

		fmt.Printf("%s\n", body)

		var a AutoGenerated
		err = json.Unmarshal(body, &a)
		if err != nil || a.Ts == 0 {
			a.Failed = 3
			fmt.Println("Error! Something is really broken")
		}

		switch a.Failed {
		case 0:
			ctx.TS = a.Ts
			for i := range a.Updates {
				if a.Updates[i].Type == "message_new" {
					//fmt.Printf("%s\n", body)
					ctx.PeerID = a.Updates[i].Object.Message.PeerID
					ctx.UserID = a.Updates[i].Object.Message.FromID
					ctx.MessID = a.Updates[i].Object.Message.ConversationMessageID
					go routine(*ctx, a.Updates[i].Object.Message.Text)
				}
			}
		case 1, 2, 3:
			err = ctx.InitLongPoll(ctx.AccessToken, ctx.GroupID)
			if err != nil {
				return err
			}
		}
	}
}
